require! {
  fs
}
vstranaLines = fs.readFileSync "#__dirname/../data/strany.tsv"
  .toString!
  .replace /\r/g ""
  .split "\n"
  .filter -> it.length > 2
nameToVstrana = {}
fullNameToVstrana = {}
zkratkaToVstrana = {}
vstranaToName = {}
idsToNames = {}
for line in vstranaLines
  [vstrana, n120, n50, n30, zkratka] = line.split "\t"
  vstrana = parseInt vstrana, 10
  nameToVstrana[n30] = vstrana
  zkratkaToVstrana[zkratka] = vstrana
  fullNameToVstrana[n120] = vstrana
  vstranaToName[vstrana] = n30.replace /"""/g '"'
  idsToNames[vstrana] = vstranaToName[vstrana]
nameToVstrana["Strana venkova"] = 101
nameToVstrana["Evropští demokraté"] = 311
nameToVstrana["SNK sdružení nezávislých"] = 1009
nameToVstrana["Pravý Blok"] = 261
lastNewId = 1000
for year in [2000 2004]
  colsToIds = {}
  lines = fs.readFileSync "#__dirname/../data/kraje_#{year}_nazvy.tsv"
    .toString!
    .replace /\r/g ""
    .split "\n"
    .filter -> it.length > 2
  lines.shift!

  for line in lines
    [id, col, name] = line.split "\t"
    if name == "Čtyřkoalice"
      name = "Křesť.demokr.unie-Čs.str.lid."
    if nameToVstrana[name] is void
      nameToVstrana[name] = ++lastNewId
    id = nameToVstrana[name]
    idsToNames[id] ?= name
    colsToIds[col] = id
  cols = "col\tid"
  for col, id of colsToIds
    cols += "\n#col\t#id"
  fs.writeFileSync "#__dirname/../data/strany_cols_#year.tsv", cols


year = 2008
colsToIds = {}
lines = fs.readFileSync "#__dirname/../data/kraje_#{year}_nazvy.tsv"
  .toString!
  .replace /\r/g ""
  .split "\n"
  .filter -> it.length > 2
lines.shift!
kstranaToVstrana = {}
for line in lines
  [kstrana, zkratka, nazev] = line.split "\t"
  if zkratkaToVstrana[zkratka] isnt void
    kstranaToVstrana[kstrana] = zkratkaToVstrana[zkratka]
  else
    if fullNameToVstrana[nazev] isnt void
      kstranaToVstrana[kstrana] = fullNameToVstrana[nazev]
    else
      kstranaToVstrana[kstrana]  = ++lastNewId
  id = kstranaToVstrana[kstrana]
  idsToNames[id] ?= nazev

fs.readFileSync "#__dirname/../data/main_2012.tsv"
  .toString!
  .replace /\r/g ""
  .split "\n"
  .0
  .split "\t"
  .map ->
    m = it.match /STRANA_([0-9]+)_PROC_HLASU/
    if m then m.1 else null
  .filter -> it
  .forEach ->
    idsToNames[it] ?= vstranaToName[it]



out = "kstrana\tvstrana"
for kstrana, vstrana of kstranaToVstrana
  out += "\n#kstrana\t#vstrana"
fs.writeFileSync "#__dirname/../data/kstranyToVstrany.tsv", out

ids = "id\tname"
for id, name of idsToNames
  ids += "\n#id\t#name"
fs.writeFileSync "#__dirname/../data/strany_id.tsv", ids
