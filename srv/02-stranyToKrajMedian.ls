require! {
  fs
  "simple-statistics": stats
}
out = "rok\tkraj\tstrana\tmedian\tdev"

obceToKraje = {}
obceToKraje[558443] = "Jihomoravský kraj"
obceToKraje[558419] = "Olomoucký kraj"
obceToKraje[500020] = "Olomoucký kraj"
obceToKraje[500011] = "Zlínský kraj"
obceToKraje[500046] = "Moravskoslezský kraj"
krajeToNuts =
  "Středočeský kraj": "CZ020"
  "Jihočeský kraj": "CZ031"
  "Plzeňský kraj": "CZ032"
  "Karlovarský kraj": "CZ041"
  "Ústecký kraj": "CZ042"
  "Liberecký kraj": "CZ051"
  "Královéhradecký kraj": "CZ052"
  "Pardubický kraj": "CZ053"
  "Vysočina": "CZ063"
  "Jihomoravský kraj": "CZ064"
  "Olomoucký kraj": "CZ071"
  "Zlínský kraj": "CZ072"
  "Moravskoslezský kraj": "CZ080"

for year in [2000 2004]
  colsToIds = {}
  fs.readFileSync "#__dirname/../data/strany_cols_#year.tsv"
    .toString!
    .replace /\r/g ""
    .split "\n"
    .slice 1
    .forEach ->
      [col, id] = it.split "\t"
      colsToIds[col] = id

  data = fs.readFileSync "#__dirname/../data/kraje_#{year}_main.tsv"
    .toString!
    .replace /\r/g ""
    .split "\n"
  [header, ...lines] = data
  header = header.split "\t" .slice 3
  lines.pop!
  sums = {}
  resultsByKrajAndParty = {}
  for line in lines
    [kraj, obec, obecId, ...results] = line.split "\t"
    if kraj == "Ostravský kraj" => kraj = "Moravskoslezský kraj"
    if kraj == "Brněnský kraj" => kraj = "Jihomoravský kraj"
    if kraj == "Jihlavský kraj" => kraj = "Vysočina"
    if kraj == "Budějovický kraj" => kraj = "Jihočeský kraj"
    krajId = krajeToNuts[kraj]
    obceToKraje[obecId] = kraj
    results .= map -> if it then parseFloat it else null
    sum = 0
    for result in results.filter(-> it)
      sum += result

    for col, index in header
      party = colsToIds[col]
      party = parseInt party, 10
      party = 156 if party in [645 632 153 320] # DS
      value = results[index]
      continue if value is null
      percentage = value / sum
      resultsByKrajAndParty["#{krajId}-#{party}"] ?= []
      resultsByKrajAndParty["#{krajId}-#{party}"].push percentage
    sums[obecId] = sum


  for krajParty, results of resultsByKrajAndParty
    results .= filter -> not isNaN it
    results.sort (a, b) -> a - b
    [kraj, party] = krajParty.split "-"
    median = stats.median results .toString!
    if median.length > 6
      median.= substr 0, 6
    dev = stats.medianAbsoluteDeviation results .toString!
    continue if dev == "0"
    # if kraj == "CZ071" and party == "55"
    #   console.log results
    if dev.length > 6
      dev.= substr 0, 6
    out += "\n#year\t#kraj\t#party\t#median\t#dev"


kstranaToVstrana = {}
fs.readFileSync "#__dirname/../data/kstranyToVstrany.tsv"
  .toString!
  .split "\n"
  .slice 1
  .forEach ->
    [kstrana, vstrana] = it.split "\t"
    kstranaToVstrana[kstrana] = vstrana


for year in  [2008 2012]
  data = fs.readFileSync "#__dirname/../data/main_#{year}.tsv"
    .toString!
    .replace /\r/g ""
    .split "\n"
  [header, ...lines] = data
  stranaHeaders = []
  for col, index in header.split "\t"
    if col.match /.STRANA_([0-9]+)_PROC_HLASU/
      party = that.1
      if year == 2008
        party = kstranaToVstrana[party]
      party = parseInt party, 10
      party = 156 if party in [645 632 153 320] # DS
      stranaHeaders.push {party, index}


  lines.pop!
  for line in lines
    fields = line.split "\t"
    obecId = fields[0]
    kraj = obceToKraje[obecId]
    krajId = krajeToNuts[kraj]
    console.log obecId if not kraj
    for {party, index}:sh in stranaHeaders
      continue unless fields[index].length
      percentage = 0.01 * parseFloat fields[index]
      console.log kraj if not krajId
      resultsByKrajAndParty["#{krajId}-#{party}"] ?= []
      resultsByKrajAndParty["#{krajId}-#{party}"].push percentage
  for krajParty, results of resultsByKrajAndParty
    results .= filter -> not isNaN it
    results.sort (a, b) -> a - b
    [kraj, party] = krajParty.split "-"
    median = stats.median results .toString!
    if median.length > 6
      median.= substr 0, 6
    dev = stats.medianAbsoluteDeviation results .toString!
    continue if dev == "0"
    if dev.length > 6
      dev.= substr 0, 6
    out += "\n#year\t#kraj\t#party\t#median\t#dev"

  # console.log resultsByKrajAndParty

fs.writeFileSync "#__dirname/../data/strany-mediany.tsv", out

# console.log resultsByKrajAndParty["Ostravský kraj-47"].join "\n"
