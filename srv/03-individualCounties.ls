require! fs
require! {
  fs
  "simple-statistics": stats
}
out = "rok\tkraj\tstrana\tmedian\tdev"
obceAssoc = {}

for year in [2000 2004]
  colsToIds = {}
  fs.readFileSync "#__dirname/../data/strany_cols_#year.tsv"
    .toString!
    .replace /\r/g ""
    .split "\n"
    .slice 1
    .forEach ->
      [col, id] = it.split "\t"
      colsToIds[col] = id

  data = fs.readFileSync "#__dirname/../data/kraje_#{year}_main.tsv"
    .toString!
    .replace /\r/g ""
    .split "\n"
  [header, ...lines] = data
  header = header.split "\t" .slice 3
  lines.pop!
  resultsByKrajAndParty = {}
  for line in lines
    [kraj, obec, obecId, ...results] = line.split "\t"
    obceAssoc[obecId] ?= {}
    obecData = obceAssoc[obecId]

    results .= map -> if it then parseFloat it else null
    sum = 0
    for result in results.filter(-> it)
      sum += result

    for col, index in header
      party = colsToIds[col]
      party = parseInt party, 10
      party = 156 if party in [645 632 153 320] # DS
      value = results[index]
      continue if value is null
      percentage = value / sum
      percentage .= toString!
      if percentage.length > 6
        percentage .= substr 0, 6
      obecData[party] ?= [null, null, null, null]
      if year == 2000
        obecData[party][0] = percentage
      else
        obecData[party][1] = percentage

kstranaToVstrana = {}
fs.readFileSync "#__dirname/../data/kstranyToVstrany.tsv"
  .toString!
  .split "\n"
  .slice 1
  .forEach ->
    [kstrana, vstrana] = it.split "\t"
    kstranaToVstrana[kstrana] = vstrana

for year in [2008 2012]
  data = fs.readFileSync "#__dirname/../data/main_#{year}.tsv"
    .toString!
    .replace /\r/g ""
    .split "\n"
  [header, ...lines] = data
  stranaHeaders = []
  for col, index in header.split "\t"
    if col.match /STRANA_([0-9]+)_PROC_HLASU/
      party = that.1
      if year == 2008
        party = kstranaToVstrana[party]
      party = parseInt party, 10
      party = 156 if party in [645 632 153 320] # DS
      stranaHeaders.push {party, index}


  lines.pop!

  for line in lines
    fields = line.split "\t"
    obecId = fields[0]
    obceAssoc[obecId] ?= {}
    obecData = obceAssoc[obecId]
    for {party, index} in stranaHeaders
      continue unless fields[index].length
      percentage = 0.01 * parseFloat fields[index]
      percentage .= toString!
      if percentage.length > 6
        percentage .= substr 0, 6
      obecData[party] ?= [null, null, null, null]
      if year == 2008
        obecData[party][2] = percentage
      else
        obecData[party][3] = percentage


for obecId, obecData of obceAssoc
  file = "strana\t2000\t2004\t2008\t2012"
  for stranaId, years of obecData
    years .= map -> if it != null then it else ""
    if stranaId == "208"
      stranaId = 1
    file += "\n#stranaId\t#{years.join '\t'}"
  fs.writeFileSync "#__dirname/../www/data/obce/#obecId.tsv", file
