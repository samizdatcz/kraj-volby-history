class ig.Strany
  ->
    @assoc = {}
    for {id, name} in d3.tsv.parse ig.data.nazvy
      id = parseInt id, 10
      @assoc[id] = new Strana id, name
    for {kraj, dev, median, rok, kraj, strana} in d3.tsv.parse ig.data.mediany
      if not @assoc[strana]
        console.log strana
      @assoc[strana].addResult do
        parseInt rok, 10
        parseFloat median
        parseFloat dev
        kraj

class Strana
  (@id, @name) ->
    @yearsAssoc = {}


  addResult: (year, median, dev, kraj) ->
    @yearsAssoc[year] ?= {}
    @yearsAssoc[year][kraj] = {median, dev}
