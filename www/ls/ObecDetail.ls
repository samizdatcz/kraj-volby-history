yearFields = [2000 2004 2008 2012]

class ig.ObecDetail
  (@container, @stranyDb, @obecDb) ->
    @element = @container.append \div
      ..attr \class \obec-detail
    @header = @element.append \h1
      ..html "Obec<span class='suggestion-container'></span> okres <span class='okres'></span>"
    @headerSuggesterContainer = @header.select \.suggestion-container
    @headerOkres = @header.select \.okres
    @results = @element.append \div
      ..attr \class \results
    @tops = @results.append \div
      ..attr \class \tops
      ..append \h2
        ..html "Nejúspěšnější strany"
    @topList = @tops.append \ul
    @bottoms = @results.append \div
      ..attr \class \bottoms
      ..append \h2
        ..html "Nejméně úspěšné strany"
    @bottomList = @bottoms.append \ul


  displayObec: (obecId) ->
    obec = @obecDb.assoc[obecId]
    (err, strany) <~ @getStrany obecId
    if err
      console.log err
      alert "Jejda, tahle obec se nám nedaří zobrazit. Zkuste to později nebo nám dejte vědět na @dataRozhlas. Díky!"
      return

    @headerOkres.html obec.okres.nazev
    tops = strany
      .filter -> it.diffAvg > 0
      .slice 0, 10
    bottoms = strany.reverse!
      .filter -> it.diffAvg < 0
      .slice 0, 10
    stranyLists = [tops, bottoms]
    [@topList, @bottomList].forEach (list, index) ->
      arr = stranyLists[index]
      list
        ..html ""
        ..selectAll \li .data arr .enter!append \li
          ..attr \data-tooltip ->
            str = "<b>#{it.strana.name} v obci #{obec.nazev}</b>"
            it.years.forEach ->
              str += "<br>V roce #{it.year} zde získala #{ig.utils.formatNumber it.result * 100, 2} % hlasů, v kraji měla mediánově #{ig.utils.formatNumber it.median * 100, 2}±#{ig.utils.formatNumber it.dev * 100, 2} %"
            str

          ..append \span
            ..attr \class \name
            ..html -> "#{it.strana.name}"
          ..append \span
            ..attr \class \result
            ..html -> formatNumber it.diffAvg


  getStrany: (obecId, cb) ->
    obec = @obecDb.assoc[obecId]
    (err, data) <~ d3.tsv "data/obce/#obecId.tsv", @rowParser
    if err
      return cb err
      return
    minDiff = Infinity
    maxDiff = -Infinity
    strany = data.map (stranaRaw) ~>
      strana = @stranyDb.assoc[stranaRaw.strana]
      if not strana
        console.log stranaRaw
      years = for year in yearFields
        result = stranaRaw[year]
        continue if result == null
        continue if not strana.yearsAssoc[year]
        continue if not strana.yearsAssoc[year][obec.krajId]
        {median, dev} = strana.yearsAssoc[year][obec.krajId]
        diff = (result - median) / dev
        maxDiff := diff if diff > maxDiff
        minDiff := diff if diff < maxDiff
        absDiff = Math.abs diff
        {year, result, diff, absDiff, median, dev}
      if years.length
        diffAvg = 0
        for year in years
          diffAvg += year.diff
        diffAvg /= years.length
      else
        diffAvg = null

      {strana, years, diffAvg}
    strany .= filter -> it.diffAvg isnt null
    strany.sort (a, b) -> b.diffAvg - a.diffAvg
    strany.minDiff = minDiff
    strany.maxDiff = maxDiff
    cb null, strany

  rowParser: (row) ->
    for key, value of row
      if value and value != "0"
        row[key] = parseFloat value
      else
        row[key] = null
    row

formatNumber = ->
  formatted = ig.utils.formatNumber it, 2
  if it > 0
    formatted = "+#formatted"
  formatted
