class ig.Obce
  ->
    @suggestions = []
    @assoc = {}
    @processSuggestions!

  processSuggestions: ->
    text = ig.data.obce
    text .= replace /\r/g ""
    [okresy, obce] = text.split "\n\n"
    okresy_assoc = {}
    okresy.split "\n"
      .map (.split "\t")
      .forEach ([kod, nazev]) -> okresy_assoc[kod] = {kod, nazev}
    @suggestions = for line in obce.split "\n"
      continue unless line.length > 2
      [id, okres_kod, nazev] = line.split "\t"
      okres = okresy_assoc[okres_kod]
      krajId = okres_kod.substr 0, 5
      id = parseInt id, 10
      nazevSearchable = nazev.toLowerCase!
      obec = {id, okres, krajId, nazev, nazevSearchable}
      @assoc[id] = obec
      obec
    @suggestions.sort (a, b) -> a.id - b.id
