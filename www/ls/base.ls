obce = new ig.Obce
container = d3.select ig.containers.base
strany = new ig.Strany

randomObec = obce.suggestions[Math.floor Math.random! * obce.suggestions.length]
if window.location.hash
  hash = window.location.hash.substr 1
  if obce.assoc[hash]
    randomObec = that
obecDetail = new ig.ObecDetail container, strany, obce

suggester = new ig.Suggester obecDetail.headerSuggesterContainer
  ..suggestions = obce.suggestions
  ..on \selected ->
    console.log window.location.hash
    window.location.hash = it.id
    obecDetail.displayObec it.id
  ..input.node!value = randomObec.nazev
obecDetail.headerSuggesterContainer.append \label
  ..attr \for \frm-suggester
new Tooltip!watchElements!
obecDetail.displayObec randomObec.id
